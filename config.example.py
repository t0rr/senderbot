import aiosocksy

API_TOKEN = 'BOT TOKEN HERE'

PROXY_LOGIN = ''
PROXY_PASSWD = ''
PROXY_URL = 'socks5://PROXY_IP:PROXY_PORT'  # or None
PROXY_AUTH = aiosocksy.Socks5Auth(login=PROXY_LOGIN, password=PROXY_PASSWD)  # or None

db_settings = {
    "host": "127.0.0.1",
    "db": "db_name",
    "user": "user_name",
    "password": "password",

    "charset": "utf8mb4",
    "use_unicode": True,
}

TEXT = """
TEST TEXT LINE 1
TEST TEXT LINE 2
TEST TEXT LINE 3
"""
