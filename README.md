# Sender Bot
[![Supported python versions](https://img.shields.io/pypi/pyversions/aiogram.svg?style=flat-square)](https://pypi.python.org/pypi/aiogram)

## How to use
1) Rename config.example.py to config.py and set it up
2) Run bot.py with python 3.6
