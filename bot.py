import asyncio
import logging
import sys

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils.exceptions import BadRequest, Unauthorized, TelegramAPIError, RetryAfter
from aiogram.utils.executor import start_polling

import config
from adb import DbHelper

logging.basicConfig(format='%(asctime)s | %(name)s:%(lineno)d | %(levelname)s | %(message)s',
                    level=logging.INFO, stream=sys.stdout)
logger = logging.getLogger(__name__)

loop = asyncio.get_event_loop()
bot = Bot(token=config.API_TOKEN, loop=loop, parse_mode=types.ParseMode.HTML, proxy=config.PROXY_URL,
          proxy_auth=config.PROXY_AUTH)
dp = Dispatcher(bot)


@dp.message_handler(types.ChatType.is_private, commands=['notify_all'])
async def notify_all(message: types.Message):
    logger.debug(f'Received /notify_all command by {message.from_user}')

    async with DbHelper() as db:
        query = "SELECT Id FROM users"
        user_rows = await db.fetchall(query)

    if not user_rows:
        logger.error('Users not found')
        return

    good = 0
    total = len(user_rows)

    for user in user_rows:
        user_id = user[0]

        try:
            await bot.send_message(user_id, config.TEXT, disable_web_page_preview=True)

        except RetryAfter as e:
            logger.warning(f'RetryAfter for {user_id}: {e}')
            await asyncio.sleep(e.timeout)

        except Unauthorized as e:
            logger.info(f'Unauthorized for {user_id}: {e}')

        except BadRequest as e:
            logger.info(f'BadRequest for {user_id}: {e}')

        except TelegramAPIError as e:
            logger.warning(f'TelegramAPIError for {user_id}: {e}')

        else:
            good += 1
            logger.info(f'Message delivered to {user_id}')

        finally:
            await asyncio.sleep(0.3)

    await message.reply(f'Done. \n'
                        f'Successful: {good} \n'
                        f'Total: {total} \n')


@dp.message_handler()
async def other_messages(message: types.Message):
    user = message.from_user
    chat = message.chat
    logger.info(f'Received {message.chat.type} ({chat.id}) message by {user.full_name} ({user.id}): {message.text}')
    await message.reply(config.TEXT, disable_web_page_preview=True)


if __name__ == '__main__':
    start_polling(dp, loop=loop, skip_updates=True)
